function fetchRandomNumbers() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0;
            if (randomNum > 0) {
                resolve(randomNum);
            } else {
                reject("Error");
            }
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    });
}

function fetchRandomString() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let result = '';
            let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            let charactersLength = characters.length;
            for (let i = 0; i < 5; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            if (result !== '') {
                resolve(result);
            } else {
                reject("Error");
            }
        }, (Math.floor(Math.random() * (5)) + 1) * 1000);
    });
}

/*#############################
        Task One
###############################*/

fetchRandomNumbers()
    .then((randomNumber) => {
        return randomNumber;
    })
    .catch((error) => {
        console.error(`Oops!! ${error} occurred while fetching randomNumber`);
    });

fetchRandomString()
    .then((randomString) => {
        return randomString;
    })
    .catch((error) => {
        console.error(`Oops!! ${error} occurred while fetching randomString`);
    });


/*#############################
        Task Two
###############################*/

fetchRandomNumbers()
    .then((randomNumber) => {
        let sum = randomNumber;
        return sum;
    })
    .then((sum) => {
        fetchRandomNumbers()
            .then((randomNumber) => {
                sum += randomNumber;
                return sum;
            })
            .catch((error) => {
                console.error(`Oops!! ${error} occurred while fetching randomNumber`);
            });
    })
    .catch((error) => {
        console.error(`Oops!! ${error} occurred while fetching randomNumber`);
    });


/*#############################
        Task Three
###############################*/

Promise.all([fetchRandomNumbers(), fetchRandomString()])
    .then((randomNumberAndString) => {
        let concatenatedString = randomNumberAndString[0] + randomNumberAndString[1];
        return concatenatedString;
    })
    .catch((error) => {
        console.error(`Oops!! ${error} occurred while fetching randomNumber and randomString`);
    });

/*#############################
        Task Four
###############################*/

const tenRandomNumberPromises = (() => {
    const promiseArray = [];
    for (let i = 0; i < 10; i++) {
        promiseArray.push(fetchRandomNumbers());
    }
    return promiseArray;
});

Promise.all(tenRandomNumberPromises())
    .then((tenRandomNumbers) => {
        let sum = tenRandomNumbers.reduce((accumulator, currentValue) => accumulator + currentValue);
        return sum;
    })
    .catch((error) => {
        console.error(`Oops!! ${error} occurred while fetching randomNumber`);
    });



/*###########################################
    Refactored Task One Using async-Await
#############################################*/

async function refactoredTaskOneFetchingRandomNumber() {
    try {
        const randomNumber = await fetchRandomNumbers();
        return randomNumber;
    } catch (error) {
        console.error(`Oops!! ${error} occurred while fetching randomNumber`);
    }
}


/*############################################
    Refactored Task Two Using async-await
#############################################*/

async function refactoredTaskTwoSumOfRandomNumbers() {
    try {
        let sum = 0;
        let randomNumber = await fetchRandomNumbers();
        sum += randomNumber;
        randomNumber = await fetchRandomNumbers();
        sum += randomNumber;
        return sum;
    } catch (error) {
        console.error(`Oops!! ${error} occurred while fetching randomNumber`);
    }
}


/*############################################
    Refactored Task Three Using async-await
#############################################*/

async function refactoredTaskThreeConcatenateRandomNumberAndString() {
    try {
        let randomNumberAndString = await Promise.all([fetchRandomNumbers(), fetchRandomString()]);
        let concatenatedString = randomNumberAndString[0] + randomNumberAndString[1];
        return concatenatedString;
    } catch (error) {
        console.error(`Oops!! ${error} occurred while fetching randomNumber or randomString`);
    }
}


/*############################################
    Refactored Task Four Using async-await
#############################################*/

async function refactoredTaskFourTenConcurrentRandomNumbers() {
    try {
        let tenRandomNumbers = await Promise.all(tenRandomNumberPromises());
        let sum = tenRandomNumbers.reduce((accumulator, currentValue) => accumulator + currentValue);
        return sum;
    } catch (error) {
        console.error(`Oops!! ${error} occurred while fetching randomNumber`);
    }
}

refactoredTaskOneFetchingRandomNumber();
refactoredTaskTwoSumOfRandomNumbers();
refactoredTaskThreeConcatenateRandomNumberAndString();
refactoredTaskFourTenConcurrentRandomNumbers();
