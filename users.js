function fetchData(data) {
    return new Promise(resolve => setTimeout(() => {
        resolve(data)
    }, 1000))
}

function sendUserLoginRequest(userId) {
    return fetchData(userId).then(() => {
        return userId;
    })
}

function getUserProfile(userId) {
    return fetchData({
        user1: {
            name: 'Vijay',
            points: 100
        },
        user2: {
            name: 'Sahana',
            points: 200
        }
    }).then(profiles => {
        return profiles[userId];
    });
}

function getUserPosts(userId) {
    return fetchData({
        user1: [{
            id: 1,
            title: 'Economics 101'
        }, {
            id: 2,
            title: 'How to negotiate'
        }],
        user2: [{
            id: 3,
            title: 'CSS Animations'
        }, {
            id: 4,
            title: 'Understanding event loop'
        }]
    }).then(posts => {
        return posts[userId];
    });
}


/**
 * Task 1: Send a login request for user1 -> get user profile data -> get user posts data
 */

async function userDataSerial() {
    try {
        const user = await sendUserLoginRequest('user1');
        const userProfile = await getUserProfile(user);
        const userPosts = await getUserPosts(user);
        return {
            userPosts,
            userProfile
        };
    } catch (error) {
        console.error("Error occurred while receiving user posts. ", error);
    }
}

/**
 * Task 2: Send a login request for user1 -> get user profile data and get user posts data simultaneously
 */

async function userDataParallel() {
    try {
        const user = await sendUserLoginRequest('user1');
        const userProfile = getUserProfile(user);
        const userPosts = getUserPosts(user);
        const userProfileAndPosts = await Promise.all([userProfile, userPosts]);
        return userProfileAndPosts;

    } catch (error) {
        console.error("Error occurred while receiving user profile or user posts. ", error);
    }
}

userDataSerial();
userDataParallel();
